## Build and run locally

* Execute `mvn clean install`
* Execute `java -jar target/coding-test-0.0.1-SNAPSHOT.jar `

## Run in docker
* Execute `mvn clean install`
* Execute `docker build . --tag app`
* Execute `docker run --rm  -p 8081:8081 app`

## Testing 
* Execute `curl -v http://localhost:8081/api/v1/festivals` 

## Profile
* Profile can be selected by providing jvm argument like `-Dspring.profiles.active=prod` which will run the server on port 9999. This is done to make application behave different on various environments. 
