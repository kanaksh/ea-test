package au.com.energyaustralia.codingtest

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class CodingTestApplication {

    static void main(String[] args) {
        SpringApplication.run(CodingTestApplication, args)
    }

}
