package au.com.energyaustralia.codingtest.api

import au.com.energyaustralia.codingtest.api.model.Label
import au.com.energyaustralia.codingtest.service.FestivalService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

/**
 * Main API class exposing endpoint.
 */
@RestController
class FestivalApi {

    @Autowired
    private FestivalService festivalService

    /**
     * Endpoint to fetch all the festivals.
     * @return
     */
    @GetMapping(path = "/api/v1/festivals", produces = "application/json")
    List<Label> getAllFestivals() {
        return festivalService.getFestivals()
    }
}
