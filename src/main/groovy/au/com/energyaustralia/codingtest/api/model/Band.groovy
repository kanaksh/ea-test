package au.com.energyaustralia.codingtest.api.model

class Band {

    String name
    List<Festival> festivals
}
