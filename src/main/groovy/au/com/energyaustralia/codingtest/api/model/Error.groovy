package au.com.energyaustralia.codingtest.api.model

class Error {
    String dateTime
    String code
    String description
    String status

}
