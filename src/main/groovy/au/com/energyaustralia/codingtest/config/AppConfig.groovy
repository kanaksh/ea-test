package au.com.energyaustralia.codingtest.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.client.RestTemplate

/**
 * Application configuration class.
 */
@Configuration
class AppConfig {

    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate()

    }
}
