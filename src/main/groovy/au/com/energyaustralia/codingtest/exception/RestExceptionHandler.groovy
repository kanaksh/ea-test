package au.com.energyaustralia.codingtest.exception

import au.com.energyaustralia.codingtest.api.model.Error
import ch.qos.logback.classic.Logger
import com.fasterxml.jackson.databind.exc.InvalidFormatException
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.client.HttpClientErrorException

import java.time.ZonedDateTime

/**
 * Controller advice for handling exceptions throughout the application.
 */
@ControllerAdvice
class RestExceptionHandler {

    final static Logger LOG = LoggerFactory.getLogger(RestExceptionHandler)
    private enum STATUS  {
        SUCCESS,
        FAILURE
    }

    /**
     * Exception handler for HTTP errors while invoking service
     * @param exception
     * @return Error object
     */
    @ExceptionHandler([HttpClientErrorException, InvalidFormatException])
    ResponseEntity<Error> handleHttpExceptions(Exception exception) {
        // do not leak the real exception details
        LOG.error("An HTTP error occurred while inkoking API : [${exception.message}]")
        return ResponseEntity.internalServerError()
                .body(
                    new Error(
                            dateTime: ZonedDateTime.now().toString(),
                            code: HttpStatus.SERVICE_UNAVAILABLE.toString(),
                            description: 'An error was returned rom downstream service',
                            status: STATUS.FAILURE.toString()
                    )
            )
    }

    /**
     * Exception handler for all other errors
     * @param exception
     * @return Error object
     */
    @ExceptionHandler(Exception)
    ResponseEntity<Error> handleException(Exception exception) {
        LOG.error("An unexpected error occurred : [${exception.message}]")
        return ResponseEntity.internalServerError()
                .body(
                    new Error(
                        dateTime: ZonedDateTime.now().toString(),
                        code: HttpStatus.SERVICE_UNAVAILABLE.toString(),
                        description: 'An unexpected error has occurred',
                        status: STATUS.FAILURE.toString()
                    )
                )
    }
}
