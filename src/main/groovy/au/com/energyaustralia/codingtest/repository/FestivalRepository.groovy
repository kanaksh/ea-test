package au.com.energyaustralia.codingtest.repository

import au.com.energyaustralia.codingtest.repository.domain.FestivalResponse

interface FestivalRepository {

    List<FestivalResponse> getAll()
}