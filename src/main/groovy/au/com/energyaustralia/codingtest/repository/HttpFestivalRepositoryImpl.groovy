package au.com.energyaustralia.codingtest.repository

import au.com.energyaustralia.codingtest.repository.domain.FestivalResponse
import ch.qos.logback.classic.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Repository
import org.springframework.web.client.RestTemplate

import static org.springframework.http.HttpStatus.OK

/**
 * Http based repository for fetching the Festival data
 */
@Repository
class HttpFestivalRepositoryImpl implements FestivalRepository {

    private final static Logger LOG = LoggerFactory.getLogger(HttpFestivalRepositoryImpl)

    @Value('${festivals.api.endpoint}')
    private String endpoint

    @Autowired
    private RestTemplate restTemplate

    /**
     * Fetch data using RestTemplate
     * @return
     */
    List<FestivalResponse> getAll() {
        ResponseEntity responseEntity =  restTemplate.exchange(endpoint, HttpMethod.GET, null, new ParameterizedTypeReference<List<FestivalResponse>>() {})
        if(OK == responseEntity.statusCode) {
            LOG.info("API Invoked successfully")
            return responseEntity.body
        }

        return []
    }
}
