package au.com.energyaustralia.codingtest.repository.domain

class FestivalResponse {

    String name
    List<Band> bands

}
