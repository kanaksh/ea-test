package au.com.energyaustralia.codingtest.service

import au.com.energyaustralia.codingtest.api.model.Band
import au.com.energyaustralia.codingtest.api.model.Festival
import au.com.energyaustralia.codingtest.api.model.Label
import au.com.energyaustralia.codingtest.repository.FestivalRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * Service class responsible for invoking repository and transform data as per APIcontract
 */
@Service
class FestivalService {

    @Autowired
    private FestivalRepository festivalRepository

    /**
     * Method to convert API Response to the Service response.
     * @return
     */
    List<Label> getFestivals() {
        def serviceResponse = festivalRepository.getAll()
        //Find all Bands form response and group on the basis of record labels
        def response = serviceResponse?.collect {
            return it.bands
        }?.flatten() //Flatten List of Lists into Single List of Bands
        ?.groupBy { it.recordLabel } //Group on the basis of recordLabel
        ?.collect { // Start creating a new API response
            return new Label(
                    label: it.key,
                    bands: it.value.collect { band ->
                    new Band(
                        name: band.name,
                        //Find all the festivals this band has attended
                        festivals: serviceResponse.collect {
                            if(it.bands.find {it.name == band.name}) {
                                return new Festival(name:  it.name)
                            }
                        }?.findAll {it?.name} //Filter nulls
                    )
                }
            )
        }

        return response?.findAll {it.label}?.sort {it.label}
    }

}
