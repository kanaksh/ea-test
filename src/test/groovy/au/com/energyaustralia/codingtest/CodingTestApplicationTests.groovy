package au.com.energyaustralia.codingtest

import au.com.energyaustralia.codingtest.api.FestivalApi
import au.com.energyaustralia.codingtest.repository.FestivalRepository
import au.com.energyaustralia.codingtest.service.FestivalService
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class CodingTestApplicationTests {

    @Autowired
    private FestivalService festivalService

    @Autowired
    private FestivalRepository festivalRepository

    @Autowired
    private FestivalApi festivalApi

    @Test
    void contextLoads() {
        assertThat(festivalApi).isNotNull()
        assertThat(festivalRepository).isNotNull()
        assertThat(festivalService).isNotNull()
    }

}
